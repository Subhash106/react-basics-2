import './App.css';
import { useState } from 'react';
import Users from './components/Users/Users';
import UserInput from './components/UserInput/UserInput';

function App() {
    const [users, setUsers] = useState([
        {id: 'i1', name: 'Subhash Chandra', age: 30},
        {id: 'i2', name: 'Neelu', age: 36},
    ]);

    const addUserHandler = (userData) => {
        const newUser = {
            id: 'i' + Math.random().toString(),
            name: userData.name,
            age: userData.age
        }

        setUsers((prevState) => {
            return [newUser, ...prevState];
        })
    }

  return (
    <div className="App">
        <UserInput onAddUser={addUserHandler} />
        <Users users={users} />
    </div>
  );
}

export default App;
