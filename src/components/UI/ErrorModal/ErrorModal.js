import React from "react";
import ReactDom from "react-dom";

import styles from "./ErrorModal.module.css";
import Backdrop from "../Backdrop/Backdrop";
import ModalOverlay from "./ModalOverlay/ModalOverlay";

const ErrorModal = (props) => {
  const closeModalHandler = () => {
    props.onCloseModal();
  };
  return (
    <React.Fragment>
      {ReactDom.createPortal(
        <Backdrop />,
        document.getElementById("backdrop-root")
      )}
      {ReactDom.createPortal(
        <ModalOverlay
          onCloseModal={closeModalHandler}
          header={props.header}
          error={props.error}
        />,
        document.getElementById("modal-root")
      )}
    </React.Fragment>
  );
};

export default ErrorModal;
