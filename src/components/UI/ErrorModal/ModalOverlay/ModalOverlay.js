import React from "react";
import Card from '../../../UI/Card/Card';
import Button from "../../../UI/Button/Button";

import styles from "./ModalOverlay.module.css";

const ModalOverlay = (props) => {

    const closeModalHandler = () => {
        props.onCloseModal();
      };

  return (
    <Card className={styles.modal}>
      <header className={styles.header}>
        <h2>{props.header}</h2>
      </header>
      <div className={styles.content}>
        <p>{props.error}</p>
      </div>
      <footer className={styles.actions}>
        <Button onClick={closeModalHandler} type="button">
          Close
        </Button>
      </footer>
    </Card>
  );
};

export default ModalOverlay;
