import React, { useState, useRef } from "react";
import styles from "./UserInput.module.css";
import Button from "../UI/Button/Button";
import Card from "../UI/Card/Card";
import ErrorModal from "../UI/ErrorModal/ErrorModal";

const UserInput = (props) => {
  const nameInputRef = useRef();
  const ageInputRef = useRef();
//   const [name, setName] = useState("");
//   const [age, setAge] = useState("");
  const [formError, setFormError] = useState();

//   const nameChangeHandler = (event) => {
//     setName(event.target.value);
//   };

//   const ageChangeHandler = (event) => {
//     setAge(event.target.value);
//   };

  const formSubmitHandler = (event) => {
    event.preventDefault();

    const enteredName = nameInputRef.current.value;
    const enteredAge = ageInputRef.current.value;

    if (
        enteredName.length === 0 ||
        enteredAge.length === 0
    ) {
      setFormError({
        modalHeader: "Invalid Input",
        modalErrorMessage: "Name and age not be an empty string.",
      });
      return;
    }

    if (+enteredAge < 1) {
      setFormError({
        modalHeader: "Invalid Age",
        modalErrorMessage: "Age should be greater than 0.",
      });

      return;
    }

    props.onAddUser({ name: enteredName, age: enteredAge });

    nameInputRef.current.value = '';
    ageInputRef.current.value = '';

    // setAge("");
    // setName("");
  };

  const closeModalHandler = () => {
    setFormError();
  };

  return (
    <div>
      {formError && (
        <ErrorModal
          onCloseModal={closeModalHandler}
          header={formError.modalHeader}
          error={formError.modalErrorMessage}
        />
      )}
      <Card className={styles.userInput}>
        <form onSubmit={formSubmitHandler}>
          <div className={styles["form-controls"]}>
            <div className={styles["form-control"]}>
              <label>Name</label>
              <input
                type="text"
                ref={nameInputRef}
              />
            </div>
            <div className={styles["form-control"]}>
              <label>Age</label>
              <input
                type="number"
                ref={ageInputRef}
                min="1"
                step="1"
              />
            </div>
          </div>
          <Button type="submit">Add User</Button>
        </form>
      </Card>
    </div>
  );
};

export default UserInput;
