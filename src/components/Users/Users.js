import React from 'react';
import User from './User/User';

import styles from './Users.module.css';
import Card from '../UI/Card/Card';

const Users = (props) => {
    return <Card className={styles.users}>
        { props.users.map( user => <User key={user.id} name={user.name} age={user.age} />) }
    </Card>
}

export default Users;